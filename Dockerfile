FROM quay.io/openshifthomeroom/workshop-dashboard:5.0.0

USER root

COPY . /tmp/src
COPY dashboard.pug /opt/workshop/gateway/views/dashboard.pug
COPY dashboard.js  /opt/workshop/gateway/routes/dashboard.js

RUN rm -rf /tmp/src/.git* && \
    chown -R 1001 /tmp/src && \
    chgrp -R 0 /tmp/src && \
    chmod -R g+w /tmp/src

USER 1001

RUN /usr/libexec/s2i/assemble
